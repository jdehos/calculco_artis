CXXFLAGS = -std=c++11 `pkg-config --cflags artis-1.0`
LDFLAGS = `pkg-config --libs artis-1.0` -lboost_serialization
all: test.out test-context.out
%.out: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $(LDFLAGS) $< 


utiliser artis sur calculco :

```
ml purge

mkdir -p ~/tmp
cd ~/tmp
wget https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.gz
tar zxf boost_1_64_0.tar.gz
cd boost_1_64_0
./bootstrap.sh --prefix=${HOME}/opt/artis
./b2 -j16 install

cd ~/tmp
git clone https://gogs.univ-littoral.fr/jdehos/test_artis
cd test_artis
cp -R modules ~/opt/artis/
module use ~/opt/artis/modules
module load artis 

cd ~/tmp
git clone https://gogs.univ-littoral.fr/devs-lab/artis
mkdir -p artis/build
cd artis/build
BOOST_ROOT=${HOME}/opt/artis cmake -DCMAKE_INSTALL_PREFIX=${HOME}/opt/artis ..
make -j16 install

cd ~/tmp/test_artis
make

oarsub -S ./oar.sh
```


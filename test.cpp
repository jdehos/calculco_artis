/**
 * @file test/test.cpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#define CATCH_CONFIG_MAIN
//#include <test/catch.hpp>

#include "models.hpp"

#include <artis/builder/Builder.hpp>
#include <artis/builder/ModelFactory.hpp>
#include <artis/kernel/Simulator.hpp>
#include <artis/observer/Output.hpp>

#include <artis/utils/DateTime.hpp>

using namespace artis::kernel;
using namespace artis::builder;

using Creator = ObjectCreator < Model >;
using Factory = ModelFactory < Model, int, Creator >;

DECLARE_MODEL(AModel, ::Model, ::Factory);
DECLARE_MODEL(BModel, ::Model, ::Factory);
DECLARE_MODEL(RootModel, ::Model, ::Factory);

typedef artis::kernel::Simulator < RootModel,
                                   artis::utils::DoubleTime,
                                   ModelParameters,
                                   GlobalParameters > ASimulator;

typedef artis::builder::Builder < ::Factory,
                                  RootModel,
                                  artis::utils::DoubleTime,
                                  ModelParameters,
                                  GlobalParameters > ABuilder;

typedef artis::context::Context < artis::utils::DoubleTime > AContext;

class AView : public artis::observer::View < artis::utils::DoubleTime,
                                             ModelParameters >
{
public:
    AView()
    {
        selector("A::I", INT, { RootModel::A, AModel::IX });
        selector("A::B", BOOL, { RootModel::A, AModel::BX });
        selector("A::D", DOUBLE, { RootModel::A, AModel::DX });
        selector("B::I", INT, { RootModel::B, BModel::IY });
        selector("B::B", BOOL, { RootModel::B, BModel::BY });
        selector("B::D", DOUBLE, { RootModel::B, BModel::DY });
    }

    virtual ~AView()
    { }
};

typedef artis::observer::Output < artis::utils::DoubleTime,
                                  ModelParameters > AnOutput;

//TEST_CASE("Simulator_tests", "simple")
int main()
{
    GlobalParameters globalParameters;
    ModelParameters modelParameters;

    /**********************/

    // AModel a;
    // artis::kernel::Any v(&AModel::_ix);

    // a.init(0, modelParameters);

    // artis::kernel::to_value < AModel >(v, &a);

    /**********************/

    ABuilder builder("{ \"type\": \"RootModel\", "                   \
                     "\"name\": \"root\", "                          \
                     "\"states\": [ "                                \
                     "{ \"name\":\"_i\", \"type\":\"int\"} "         \
                     "],"                                            \
                     "\"submodels\": [ "                             \
                     "{ \"type\": \"AModel\", "                      \
                     "\"name\": \"_a\", "                            \
                     "\"internals\": ["                              \
                     "{ \"name\":\"_ix\", \"type\":\"int\"}, "       \
                     "{ \"name\":\"_bx\", \"type\":\"bool\"}, "      \
                     "{ \"name\":\"_dx\", \"type\":\"double\"} "     \
                     "] }, "                                         \
                     "{ \"type\": \"BModel\", "                      \
                     "\"name\": \"_b\", "                            \
                     "\"internals\": ["                              \
                     "{ \"name\":\"_iy\", \"type\":\"int\"}, "       \
                     "{ \"name\":\"_by\", \"type\":\"bool\"}, "      \
                     "{ \"name\":\"_dy\", \"type\":\"double\"} "     \
                     "], "                                           \
                     "\"externals\": ["                              \
                     "{ \"name\":\"_ix\", \"type\":\"int\"}, "       \
                     "{ \"name\":\"_bx\", \"type\":\"bool\"}, "      \
                     "{ \"name\":\"_dx\", \"type\":\"double\"} "     \
                     "], "                                           \
                     "\"states\": [ "                                \
                     "{ \"name\":\"_n\", \"type\":\"int\"} "         \
                     "] }"                                           \
                     "] }");

    AContext context(artis::utils::DateTime::toJulianDayNumber("2016-1-1"),
                     artis::utils::DateTime::toJulianDayNumber("2016-1-10"));
    ASimulator simulator(builder.build(), globalParameters);

    simulator.attachView("Root", new AView);

    ::Trace::trace().clear();

    simulator.init(0, modelParameters);
    simulator.run(context);

    std::cout << ::Trace::trace().elements().to_string() << std::endl;

    AnOutput output(simulator.observer());

    output();
    return 0;
}
